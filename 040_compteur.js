const log = Vue.createApp({}) ;

log.component('compteur',{
    data() {
        return {
            increment: 0
        }
    },
    template: '<button v-on:click="increment++"> Vous avez cliqué {{ increment }} fois </button>  '
})


log.mount('#ancre');