const log = Vue.createApp({})

log.component('competence', {
    props: ['titre','contenu'],
    data() {
        return {
            voirCompetence: true
        }
    },
    template: `
    <article class="message" v-show="voirCompetence">
        <div class="message-header">
        {{ titre }}
        <button type="button" @click="voirCompetence = false">x</button>
        </div>
        <div class="message-body">
        {{ contenu }}
        </div>
        </article>
    `
})


// On accroche notre instance de Vue à la DIV HTML demo
log.mount('#root')