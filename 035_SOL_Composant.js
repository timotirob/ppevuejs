
const log = Vue.createApp({})

// Définition d'un nouveau composant appelé `compteur`
log.component('compteur', {
    data () {
        return {
            comptage: 0
        }
    },
    template: '<button v-on:click="comptage++">Vous m\'avez cliqué {{ comptage }} fois.</button>'
})

// On accroche notre instance de Vue à la DIV HTML demo
log.mount('#demo')

