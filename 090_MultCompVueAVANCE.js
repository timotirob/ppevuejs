const monApp = Vue.createApp({
    data() {
        return {

        }
    }
}) ;

monApp.component('competence', {
    props: ['titre','contenu'],
    data() {
        return {
            voirCompetence: true,
            montreDetail: false
        }
    },
    template: `
    <article class="message" v-show="voirCompetence">
        <div class="message-header">
        {{ titre }}
        <button type="button" @click="voirCompetence = false">x</button>
        </div>
        <div class="message-body">
        {{ contenu }}
        <button @click="montreDetail=true"> Voir les détails</button>
        <div>
        <modal v-if="montreDetail" @close="montreDetail = false">
        <slot name="detail"> </slot>
        </modal>
        </div></div>
        </article>
    `
});

monApp.component('modal',{
    template: `
  <div class modal="is-active">
  <div class="modal-background"></div>
  <div class="modal-card">
    <header class="modal-card-head">
      <p class="modal-card-title">Un Titre</p>
      <button class="delete" aria-label="close"></button>
    </header>
    <div class="box">
    <P>
    <slot> </slot>
    </P>
    </div>
    <footer class="modal-card-foot">
      <button class="button is-success">Sauvegardez</button>
      <button class="button" @click="$emit('close')">Annulez</button>
    </footer>
  </div>
</div>
    `
});


monApp.mount('#root')
