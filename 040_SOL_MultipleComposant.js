const log = Vue.createApp({
    data () {
        return {
            listeSlam: [
                {
                    id: 1,
                    code: 'SLAM3',
                    libelle: 'Data'
                },
                {
                    id: 2,
                    code: 'SLAM4',
                    libelle: 'Dev'
                },
                {
                    id: 3,
                    code: 'PPE',
                    libelle: 'Projet'
                }
            ]
        }
    }
})

// Définition d'un nouveau composant appelé `afaire-item`
log.component('afaire-item', {
    props: {
        liste: Object
    },
    template: '<li> {{ liste.code }} - {{ liste.libelle }} </li>'
})

// On accroche notre instance de Vue à la DIV HTML demo
log.mount('#grandeliste')