const monApp = Vue.createApp({
    data() {
        return {
            montreDetail: false
        }
    }
}) ;

monApp.component('competence', {
    props: ['titre','contenu'],
    data() {
        return {
            voirCompetence: true
        }
    },
    template: `
    <article class="message" v-show="voirCompetence">
        <div class="message-header">
        {{ titre }}
        <button type="button" @click="voirCompetence = false">x</button>
        </div>
        <div class="message-body">
        {{ contenu }}
        </div>
        </article>
    `
})
monApp.component('modal', {
    data() {
        return {

        }
    },
    template: `
   <div class modal="is-active">
    <div class="modal-background"></div>
    <div class="modal-content">
    <div class="box">
    <P>
    <slot name="contenu"> </slot>
    </P>
    <button class="modal-close" @click="$emit('close')"></button>
    </div></div></div>
    `
})

monApp.mount('#root')
