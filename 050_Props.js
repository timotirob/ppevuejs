const log = Vue.createApp({
    data() {
        return {
            listeSlam: [
                {
                    id: 'a',
                    code: 'Bloc2',
                    libelle: 'Développement et modélisation'
                },
                {
                    id: 'b',
                    code: 'Bloc3',
                    libelle: 'Cybersécurité'
                },
                {
                    id: 'c',
                    code: 'AP',
                    libelle: 'Atelier de professionnalisation'
                },

            ]
        }
    }
}) ;

log.component('afaire',{
    props:{
        liste: Object
    },

    data() {
        return {
        }
    },
    template: '<li> {{ liste.id }} -{{ liste.code }} - {{ liste.libelle}} </li>  '
})


log.mount('#slam');